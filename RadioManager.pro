#-------------------------------------------------
#
# Project created by QtCreator 2018-04-12T16:53:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RadioManager
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32:RC_FILE += src/RadioManager.rc

SOURCES += \
        src/setting_window.cpp \
        src/settings.cpp \
        src/main.cpp \
        src/mainwindow.cpp \
        src/utils.cpp

HEADERS += \
        src/setting_window.h \
        src/settings.h \
        src/mainwindow.h \
        src/globals.h \
        src/utils.h

FORMS += \
        src/setting_window.ui \
        src/mainwindow.ui

DISTFILES += \
        src/RadioManager.rc
