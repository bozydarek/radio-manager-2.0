#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <cstring>

// Singleton
class Settings
{
private:
    Settings();

public:
    Settings(Settings const&)       = delete;
    void operator=(Settings const&) = delete;

    static Settings& getSettings();

public:
    // public methods
    bool loadSettings(std::string pathToFile=".");
};

#endif // SETTINGS_H
