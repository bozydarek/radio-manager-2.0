#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "setting_window.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void on_add_button_clicked();
    void on_export_button_clicked();

    void on_actionWyjd_triggered();
    void on_actionUstawienia_triggered();

    void on_dj_id_textEdited(const QString &arg1);

private:
    Ui::MainWindow *ui;
    setting_window *sw;
    void counter_update(int no);
    void clear_forms();
};

#endif // MAINWINDOW_H
