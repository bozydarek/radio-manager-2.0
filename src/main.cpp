#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDebug>

#include "mainwindow.h"
#include "settings.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);


    QTranslator qtTranslator;
    if (qtTranslator.load(QLocale::system(),
                "qt", "_",
                QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
    {
        qDebug() << "qtTranslator ok";
        app.installTranslator(&qtTranslator);
    }

    QTranslator qtBaseTranslator;
    if (qtBaseTranslator.load("qtbase_" + QLocale::system().name(),
                QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
    {
        qDebug() << "qtBaseTranslator ok";
        app.installTranslator(&qtBaseTranslator);
    }

    // Load settings
    Settings::getSettings().loadSettings();

    MainWindow w;
    w.show();

    return app.exec();
}
