#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "globals.h"
#include "utils.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    sw(new setting_window(this))
{
    ui->setupUi(this);

    ui->label_version->setText(tr("ver. ") + tr(VERSION));
    ui->label_version->setStyleSheet("QLabel { color : grey; }");

    ui->label_date->setText(tr("Raport z dnia: ") + QDateTime::currentDateTime().toString("d.M.yyyy"));

    // Column width setup
    int no_width = 40, id_width = 70, hour_width = 70;

    auto author_col_width = ui->history->width() - (no_width + id_width + hour_width ) - 2;
    ui->history->setColumnWidth(0, no_width); // No
    ui->history->setColumnWidth(1, author_col_width); // Author
    ui->history->setColumnWidth(2, id_width); // ID
    ui->history->setColumnWidth(3, hour_width); // Hour

    ui->history->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    int ret = QMessageBox::question(this, "",
                                    tr("Czy napewno chcesz wyjść z aplikacji?"),
                                    QMessageBox::Yes | QMessageBox::No );
    switch (ret) {
        case QMessageBox::Yes:
            // Continue closing the application
            break;
        default:
            event->ignore();
            break;
    }
}

void MainWindow::on_add_button_clicked()
{
    auto row = ui->history->rowCount()+1;

    //check if everything is ok
    auto music_desc = ui->music_input->text();
    auto class_id = ui->class_combo->currentText();
    auto dj_id = ui->dj_id->text();

    if (music_desc.length() < 1) {
        QMessageBox::warning(this, tr("Błąd danych"),
                             tr("Podaj tytuł utworu"),
                             QMessageBox::Ok);
        return;
    }

    if (dj_id.length() < 2){
        QMessageBox::warning(this, tr("Błąd danych"),
                             tr("ID DJa musi być długości 2 albo 3"),
                             QMessageBox::Ok);
        return;
    }

    //add new item
    ui->history->setRowCount(row);
    QTableWidgetItem *no = new QTableWidgetItem(QString::number(row));
    ui->history->setItem(row-1, 0, no);

    QTableWidgetItem *music = new QTableWidgetItem(music_desc);
    ui->history->setItem(row-1, 1, music);

    QTableWidgetItem *id = new QTableWidgetItem( class_id + "-" + dj_id );
    ui->history->setItem(row-1, 2, id);

    QTableWidgetItem *time = new QTableWidgetItem(QDateTime::currentDateTime().toString("H:mm"));
    ui->history->setItem(row-1, 3, time);

    // after adding new element
    ui->history->scrollToBottom();
    counter_update(row);

    // clear forms
    clear_forms();
}

void MainWindow::counter_update(int no)
{
    if(no == 0){
        ui->counter->setText(tr("Dziś nie puszczono jeszcze żadnych utworów."));
    }
    else {
        auto plural = QString(polishPlural(no, "utwór", "utwory", "utworów").c_str());
        ui->counter->setText(tr("Dziś puszczono ") + QString::number(no) + " " + plural);
    }
}

void MainWindow::clear_forms()
{
    ui->music_input->clear();
}

void MainWindow::on_export_button_clicked()
{
    ui->done_indicator->setText(tr("Wyeksportowano!"));
}

void MainWindow::on_actionWyjd_triggered()
{
    this->close();
}

void MainWindow::on_dj_id_textEdited(const QString &arg1)
{
    ui->dj_id->setText(arg1.toUpper());
}

void MainWindow::on_actionUstawienia_triggered()
{
    sw->exec();
}
