#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <cstring>

std::string polishPlural(int value, std::string singularNominativ, std::string pluralNominativ, std::string pluralGenitive);

#endif // UTILS_H
